fileName = input("Spike data filename: ", 's');
fid = fopen(fileName)
neuronId = 1;
plotArray = [];
tempArray = fgetl(fid);
while(tempArray>0)
	tempArray = sscanf(tempArray, '%d')';
	spikeArray = [tempArray; (ones(1, length(tempArray)) * neuronId)];
	plotArray = [plotArray spikeArray];
	neuronId = neuronId + 1;
	tempArray = fgetl(fid);
end
fclose(fid);
fileName = input("histogram data filename: ", 's');
fid = fopen(fileName, 'w');
for i = 1:length(plotArray)
	str = [num2str(plotArray(1, i)) ',' num2str(plotArray(2, i)) "\n"];
	fputs(fid, str);
end
fclose(fid);
