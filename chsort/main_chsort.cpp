// include CARLsim user interface
#include <iostream>
#include <carlsim.h>

#if defined(WIN32) || defined(WIN64)
#include <stopwatch.h>
#endif

#define SIMSECONDS 1
#define SIMMILLISECONDS 0
#define BITLIMIT 32
#define DELAY 4
#define EXCITSPIKE 30

using namespace std;

//Connection class for connection between
//comparator matrix and excitatory input
class ExcitConnection: public ConnectionGenerator
{
	int xLen, yLen;
public:
	ExcitConnection() {}
	ExcitConnection(int n)
	{
		xLen = yLen = n;
	}
	~ExcitConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x2 = j/xLen, y2 = j%xLen;
		connected = false;
		if(!y2)
			connected = true;
		weight = 0.105f;
		maxWt = 0.105f;
		delay = 1;
	}
};

//Connection class for connection between
//comparator matrix and inhibitory input
class InhibConnection: public ConnectionGenerator
{
	int xLen, yLen;
public:
	InhibConnection() {}
	InhibConnection(int n)
	{
		xLen = yLen = n;
	}
	~InhibConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/xLen, y1 = i%xLen;
		int x2 = j/xLen, y2 = j%xLen;
		connected = false;
		if((x1 == x2) && (y1>y2))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

class ShuttingConnection: public ConnectionGenerator
{
	int xLen, yLen;
public:
	ShuttingConnection() {}
	ShuttingConnection(int n)
	{
		xLen = yLen = n;
	}
	~ShuttingConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/xLen, y1 = i%xLen;
		int x2 = j/xLen, y2 = j%xLen;
		connected = false;
		if((x1 == x2) && (y1>=y2))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Connection class that connects the comparator matrix
//with the rank matrix
class RankConnection: public ConnectionGenerator
{
	int xLen, yLen;
public:
	RankConnection() {};
	RankConnection(int n)
	{
		xLen = yLen = n;
	}
	~RankConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/xLen, y1 = i%xLen;
		int x2 = j/xLen, y2 = j%xLen;
		connected = false;
		if(x1 == x2)
			connected = true;
		float wt = 0.105f;
		if(y2>1)
			wt = wt / (float) y2;
		weight = wt;
		maxWt = wt;
		delay = 1;
	}
};

class SignConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SignConnection() {}
	SignConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SignConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = 0, y1 = i/yLen, z1 = i%yLen;
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = (y2!=z2) && (y1!=z1) && (y1 == z2) && (z1 == y2);
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 6;
	}
};

class SingleConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SingleConnection() {}
	SingleConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SingleConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = (x1 == 0) && (y2!=z2) && (y1!=z1) && (y1 == y2);
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

class NumberCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	NumberCompConnection() {};
	NumberCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~NumberCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 == x2) && ((y2!=z2) && (y1 == y2)))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 6;
	}
};

class InhibCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	InhibCompConnection() {};
	InhibCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~InhibCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = i/yLen, y1 = i%yLen, z1 = 0;
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 == x2) && ((y2!=z2) && (y1 == z2)))
		{
			connected = true;
		}
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

class SelectCompConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	SelectCompConnection() {};
	SelectCompConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~SelectCompConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = (i/yLen)%xLen, y1 = i%yLen, z1 = i/(yLen * xLen);
		int x2 = 0, y2 = j/yLen, z2 = j%yLen;
		connected = false;
		if((y1 == y2) && (z1 == z2))
		{
			connected = true;
			//fprintf(stderr, "%d %d %d %d %d\n", x1, y1, x2, y2, z2);
		}
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

class CompSelectConnection: public ConnectionGenerator
{
	int xLen, yLen, zLen;
public:
	CompSelectConnection() {};
	CompSelectConnection(int n)
	{
		xLen = BITLIMIT;
		yLen = n;
		zLen = n;
	}
	~CompSelectConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		int x1 = (i/yLen)%xLen, y1 = i%yLen, z1 = i/(yLen * xLen);
		int x2 = (j/yLen)%xLen, y2 = j%yLen, z2 = j/(yLen * xLen);
		connected = false;
		if((x1 < x2) && (y2!=z2) && (y1 != z1) &&
			(((y1 == z2) && (y2 == z1)) || ((y1 == y2) && (z1 == z2))))
			connected = true;
		weight = 0.2f;
		maxWt = 0.2f;
		delay = 1;
	}
};

//Customised spike generator class
class TemporalSpikes: public SpikeGenerator
{
	vector<vector<int> > spikeList;
	vector<int> finishList;
public:
	TemporalSpikes(){};
	TemporalSpikes(vector<vector<int> > spikes)
	{
		for(int i=0; i<spikes.size(); i++)
		{
			vector<int> temp;
			for(int j=0; j<spikes[i].size(); j++)
			{
				temp.push_back(spikes[i][j]);
				finishList.push_back(0);
			}
			spikeList.push_back(temp);
		}
	}
	unsigned int nextSpikeTime(CARLsim *sim, int grpId, int nid, unsigned int currentTime, unsigned int lastScheduledSpikeTime,
		unsigned int endOfTimeSlice)
	{
		int x=nid/spikeList.size(), y=nid%spikeList.size();
		if(finishList[nid] || (!spikeList[y][x]))
			return SIMSECONDS * 1000 + SIMMILLISECONDS + 1;
		else
		{
			finishList[nid]++;
			return spikeList[y][x];
		}
	}
};

class LateSpikes: public SpikeGenerator
{
	int time;
public:
	LateSpikes();
	LateSpikes(int time)
	{
		this->time = time;
	}
	unsigned int nextSpikeTime(CARLsim *sim, int grpId, int nid, unsigned int currentTime, unsigned int lastScheduledSpikeTime,
		unsigned int endOfTimeSlice)
	{
		if(time<=lastScheduledSpikeTime)
			return SIMSECONDS * 1000 + SIMMILLISECONDS + 1;
		else
			return time;
	}
};

//Converts number to spike trains
vector<vector<int> > convertNumToSpike(int n, int *array)
{
	vector<vector<int> > spikeList;
	for(int i=0; i<n; i++)
	{
		vector<int> bitList;
		int temp = BITLIMIT, num = array[i], time = 1;
		while(temp>0)
		{
			bitList.push_back(num & 1);
			num = num >> 1;
			temp--;
		}
		for(int i=0; i<bitList.size()/2; i++)
		{
			int temp = bitList[i];
			bitList[i] = bitList[bitList.size() - i - 1];
			bitList[bitList.size() - i - 1] = temp;
		}
		spikeList.push_back(bitList);
	}
	return spikeList;
}

class LayerDetails
{
	int layerID;
	int layerIndex;
    string layerName;
public:
    LayerDetails()
	{
		layerID = -1;
		layerIndex = -1;
		layerName = "";
	}
	LayerDetails(int id, string name)
	{
		 layerID = id;
		 layerName = name;
	}
	void setLayerIndex(int index)
	{
	    layerIndex = index;
	}
	void setLayerID(int id)
	{
	    layerID = id;
	}
	void setLayerName(string name)
	{
	    layerName = name;
	}
	int getLayerID()
	{
		return layerID;
	}
	int getLayerIndex()
	{
	    return layerIndex;
	}
	string getLayerName()
	{
	    return layerName;
	}
}

class NetworkSolver
{
    vector<LayerDetails> network;
	CARLsim simulator;
public:
    NetworkSolver()
	{
	    int ithGPU = 0;
	    int randSeed = 42;
		simulator = CARLsim("Chen-Hsieh sort",CPU_MODE,USER,ithGPU,randSeed);
	}
	NetworkSolver(vector<LayerDetails> net)
	{
	    int ithGPU = 0;
	    int randSeed = 42;
		simulator = CARLsim("Chen-Hsieh sort",CPU_MODE,USER,ithGPU,randSeed);
	    network = net;
	}
	int searchIndexByName(string name)
	{
	    for(int i=0; i<network.size(); i++)
		    if(name == network[i].getLayerName)
			    return i;
		return -1;
	}
	int searchIndexByID(int id)
	{
	    for(int i=0; i<network.size(); i++)
		    if(id == network[i].getLayerID())
			    return i;
		return -1;
	}
	void addLayerDetail(string name, int neuron_num, int neuron_type, bool generator = false)
	{
	    LayerDetails layer;
		int id;
		if(generator)
		    id = simulator.createSpikeGeneratorGroup(name, neuron_num, neuron_type);
		else
		{
		    id = simulator.createGroup(name, neuron_num, neuron_type);
			sim.setNeuronParameters(id, 0.02f, 0.2f, -65.0f, 8.0f);
		}
		layer.setLayerIndex(network.size());
		layer.setLayerID(id);
		layer.setLayerName(name);
	    network.push_back(layer);
	}
	void addConnectionThroughSpecs(string srcName, string destName, string connType, float weight,
	        float connProb, float delay, float radius, bool synapse)
	{
	    simulator.connect(searchIndexByName(srcName), searchIndexByName(destName), connType, RangeWeight(weight),
		    connProb, RangeDelay(delay), RadiusRF(radius), synapse);
	}
	void addConnectionThroughClass(string srcName, string destName, ConnectionGenerator *conn, bool synapse)
	{
	    simulator.connect(searchIndexByName(srcName), searchIndexByName(destName), conn, synapse);
	}
	void solve()
	{
	}
}

int main(int argc, char **argv) {
	//Validate the arguments
	if(argc == 1)
	{
		printf("Usage:./%s <array-size> < <file-name>\n", argv[0]);
		return 1;
	}

	const int n = atoi(argv[1]);
	
	//Parameters for initilaising the simulator
	//Note: ithGPU isn't needed if the simulator is running in CPU mode
	//Also note from the CARLsim documentation that except for the first argument
	//all the other arguments have a default argument
	int ithGPU = 0;
	int randSeed = 42;
	CARLsim sim("Chen-Hsieh sort",CPU_MODE,USER,ithGPU,randSeed);

	int numList, inhibNumList;
	int compLayer, inhibCompLayer;
	int selectionLayer;
	int execInter, delayExecInter, inhibInter;
	int comparator, inhib_comparator, pre_rank, inhib_pre_rank;
	int rank, selection, numbers[n];
	int inter_spike;
	bool isGenerator = true;
	NetworkSolver solver;
	inter_spike = sim.createSpikeGeneratorGroup("Exciter Neuron", 1, EXCITATORY_NEURON);
	for(int i=0; i<n; i++)
		if(!scanf("%d", &numbers[i]))
			return 1;

	//Declare the connection groups for connecting neuron groups in the future
	InhibConnection inhibConn(n);
	ExcitConnection excitConn(n);
	RankConnection rankConn(n);
	NumberCompConnection numCompConn(n);
	InhibCompConnection inhibCompConn(n);
	CompSelectConnection compSelectConn(n);
	SelectCompConnection selectCompConn(n);
	ShuttingConnection shutConn(n);
	SingleConnection singleConn(n);
	SignConnection signConn(n);

	//Create the required neuron groups
	solver.addLayerDetail("Number", BITLIMIT * n, EXCITATORY_NEURON, isGenerator);
	solver.addLayerDetail("Inhibitory Number", BITLIMIT * n, INHIBITORY_NEURON, !isGenerator);
	solver.addLayerDetail("Comparator", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Comparator Inhibitor", BITLIMIT * n, INHIBITORY_NEURON, !isGenerator);
	solver.addLayerDetail("Pre-Rank matrix", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Rank matrix", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Exciter", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Delayer", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Inhibitor", BITLIMIT * n, INHIBITORY_NEURON, !isGenerator);

	/*numList = sim.createSpikeGeneratorGroup("Number", BITLIMIT * n, EXCITATORY_NEURON);
	inhibNumList = sim.createGroup("Inhibitory Number", BITLIMIT * n, INHIBITORY_NEURON);
	comparator = sim.createGroup("Comparator-Matrix", n * n, EXCITATORY_NEURON);
	inhib_comparator = sim.createGroup("Comparator-Matrix", n * n, INHIBITORY_NEURON);
	pre_rank = sim.createGroup("Pre-Rank-Layer", n * n, EXCITATORY_NEURON);
	rank = sim.createGroup("Rank", n * n, EXCITATORY_NEURON);
	execInter = sim.createGroup("excitatory neuron", n*n, EXCITATORY_NEURON);
	delayExecInter = sim.createGroup("excitatory-delayor neuron", n*n, EXCITATORY_NEURON);
	inhibInter = sim.createGroup("inhibitory-bit neuron", n*n, INHIBITORY_NEURON);*/

	solver.addLayerDetail("Inhibitory-Pre-Rank", BITLIMIT * n, INHIBITORY_NEURON, !isGenerator);
	solver.addLayerDetail("Comparison", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	solver.addLayerDetail("Inhibitory Comparison", BITLIMIT * n, INHIBITORY_NEURON, !isGenerator);
	solver.addLayerDetail("Selection", BITLIMIT * n, EXCITATORY_NEURON, !isGenerator);
	//selection = sim.createGroup("Selection-Layer " + rowstream.str(), n, EXCITATORY_NEURON);
	/*inhib_pre_rank = sim.createGroup("Inhibitory-Pre-Rank-Layer", n * n, INHIBITORY_NEURON);
	compLayer = sim.createGroup("Comparator Layer", BITLIMIT * n * n, EXCITATORY_NEURON);
	inhibCompLayer = sim.createGroup("Inhibitory Comp Layer", BITLIMIT * n * n, INHIBITORY_NEURON);
	selectionLayer = sim.createGroup("Selection Matrix", BITLIMIT * n * n, EXCITATORY_NEURON);*/

	//set the parameters for the neurons
	/*sim.setNeuronParameters(inhibNumList, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(comparator, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhib_comparator, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(pre_rank, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(rank, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhib_pre_rank, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(compLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhibCompLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(selectionLayer, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(execInter, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(delayExecInter, 0.02f, 0.2f, -65.0f, 8.0f);
	sim.setNeuronParameters(inhibInter, 0.02f, 0.2f, -65.0f, 8.0f);*/

	//connect numList with inhibitory numlist
	sim.connect(numList, inhibNumList, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);

	//connect number and comparison layer
	sim.connect(numList, compLayer, &numCompConn, SYN_FIXED);
	sim.connect(inhibNumList, compLayer, &inhibCompConn, SYN_FIXED);
	sim.connect(numList, execInter, &singleConn, SYN_FIXED);
	sim.connect(execInter, delayExecInter, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(execInter, inhibInter, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhibInter, comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(delayExecInter, comparator, &signConn, SYN_FIXED);

	//connect comparison layer and selection layer
	sim.connect(compLayer, inhibCompLayer, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(compLayer, selectionLayer, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhibCompLayer, selectionLayer, &compSelectConn, SYN_FIXED);

	//connect selection layer and comparator matrix
	sim.connect(selectionLayer, comparator, &selectCompConn, SYN_FIXED);
	
	//connect comparator and pre-rank matrix
	sim.connect(comparator, pre_rank, &rankConn, SYN_FIXED);
	sim.connect(comparator, inhib_pre_rank, &rankConn, SYN_FIXED);
	sim.connect(comparator, inhib_comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhib_comparator, comparator, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);

	//connect pre-rank matrix with rank matrix
	sim.connect(pre_rank, rank, "one-to-one", RangeWeight(0.2f), 1.0f, RangeDelay(8), RadiusRF(-1), SYN_FIXED);
	//sim.connect(pre_rank, inhib_pre_rank, "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhib_pre_rank, pre_rank, "one-to-one", RangeWeight(1.0f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
	sim.connect(inhib_pre_rank, rank, &inhibConn, SYN_FIXED);

	//connect spike-generator to pre-rank matrix
	sim.connect(inter_spike, pre_rank, &excitConn, SYN_FIXED);
	sim.connect(inter_spike, inhib_pre_rank, &excitConn, SYN_FIXED);

	//This loop is responsible for connecting all the gate neurons in a row to the
	//corresponding adder neuron in the adder matrix.
	sim.setConductances(true);

	//convert input numbers to spikes
	vector<vector<int> > testSpike;
	testSpike = convertNumToSpike(n, numbers);
	TemporalSpikes excSpike;
	LateSpikes late(EXCITSPIKE);
	excSpike = TemporalSpikes(testSpike);
	sim.setSpikeGenerator(numList, &excSpike);
	sim.setSpikeGenerator(inter_spike, &late);

	//setup the neurons, the connections and the generators.
	sim.setupNetwork();

	// set some spike monitors
	SpikeMonitor *spkMonout;
	spkMonout = sim.setSpikeMonitor(rank, "DEFAULT");

	// run for 1 second
	// at the end of runNetwork call, SpikeMonitor stats will be printed
	spkMonout->startRecording();
	cout<<"Finished Recording\n";
	sim.runNetwork(SIMSECONDS, SIMMILLISECONDS);
	spkMonout->stopRecording();
	printf("Selection\n");
	vector<vector<int> > output = spkMonout->getSpikeVector2D();
	for(int i=0; i<output.size(); i++)
	{
		if(i%n==0)
			fprintf(stderr, "\n");
		vector<int> spikeTime = output[i];
	//	fprintf(stderr, "%d %d %d ", (i/n)%BITLIMIT, i%n, i/(BITLIMIT*n));
		for(int j=0; j<spikeTime.size(); j++)
			fprintf(stderr, "%d ", spikeTime[j]);
			//fprintf(stderr, "1 ");
		if(!spikeTime.size())
			fprintf(stderr, "0 ");
	//	fprintf(stderr, "\n");
	}
	fprintf(stderr, "\n");
	return 0;
}
