#include<stdio.h>

int sgn(int u)
{
	if(u>0)
		return 1;
	return 0;
}

int limit(int l, int r)
{
	if(l<=r)
		return 1;
	return 0;
}

void algorithm(int *arr, int n)
{
	int t=0, i, j;
	int u[n][n], v[n][n], deltau[n][n];
	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			u[i][j] = v[i][j] = deltau[i][j] = 0;
	while(t<=2)
	{
		for(i=0; i<n; i++)
			for(j=0; j<n; j++)
				v[i][j] = sgn(u[i][j]);
		for(i=0; i<n; i++)
		{
			for(j=0; j<n; j++)
			{
				int k, sum=0;
				for(k=0; k<n; k++)
				{
					if(k==i)
						continue;
					sum+=v[i][k]*limit(arr[k], arr[j]);
				}
				deltau[i][j]=-1*limit(arr[i], arr[j])*(sum-1);
				u[i][j] = u[i][j] + deltau[i][j];
			}
		}
		printf("U(t+1):\n");
		for(i=0; i<n; i++)
		{
			for(j=0; j<n; j++)
				printf("%d ", u[i][j]);
			printf("\n");
		}
		printf("\nV(t):\n");
		for(i=0; i<n; i++)
		{
			for(j=0; j<n; j++)
				printf("%d ", v[i][j]);
			printf("\n");
		}
		printf("\n");
		t++;
	}
	for(i=0; i<n; i++)
	{
		for(j=0; j<n; j++)
			printf("%d ", v[i][j]);
		printf("\n");
	}
}

int main()
{
	int arr[6] = {8, 3, 12, 20, 9, 2000000};
	algorithm(arr, 6);
	return 0;
}
