This project implements the CH (Chen-Hseih) sort. The matrix
selection is the one which gives the output. The output is in
the form of a position matrix. selection[i][j] has a non-zero spike value only if
the number at position i in the input array must be place at position j in the output array