// include CARLsim user interface
#include <carlsim.h>

#define SIMSECONDS 1
#define SIMMILLISECONDS 0
#define BITLIMIT 32
#define DELAY 4
#define MEMDELAY 8
#define SYNCHROSPIKE DELAY * (BITLIMIT + 1) + 1

using namespace std;

//Connection class for connection between
//comparator matrix and excitatory input
class ExcitConnection: public ConnectionGenerator
{
public:
	ExcitConnection() {}
	~ExcitConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = (j == 0);
		weight = 0.4f;
		maxWt = 0.4f;
		delay = 1;
	}
};

//Connection class for connection between
//comparator matrix and inhibitory input
class InhibConnection: public ConnectionGenerator
{
public:
	InhibConnection() {}
	~InhibConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = i>j;
		weight = 0.4f;
		maxWt = 0.4f;
		delay = 1;
	}
};

//Connection class that connects the comparator matrix
//with the rank matrix
class RankConnection: public ConnectionGenerator
{
public:
	RankConnection() {};
	~RankConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = true;
		float wt = 0.12f;
		if(j>1)
			wt = wt / (float) j;
		weight = wt;
		maxWt = wt;
		delay = 1;
	}
};

class SingleConnection: public ConnectionGenerator
{
public:
	SingleConnection() {};
	~SingleConnection() {};
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = (i == 0);
		weight = 0.4f;
		maxWt = 0.4f;
		delay = 6;
	}
};

class FinalConnection: public ConnectionGenerator
{
	int idy;
public:
	FinalConnection() {};
	FinalConnection(int y)
	{
		idy = y;
	}
	~FinalConnection() {};
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = (j == idy);
		weight = 0.4f;
		maxWt = 0.4f;
		delay = 6;
	}
};

class RowConnection: public ConnectionGenerator
{
public:
	RowConnection() {};
	~RowConnection() {}
	void connect(CARLsim *sim, int srcGrp, int i, int dstGrp, int j, float &weight, float &maxWt,
		float &delay, bool &connected)
	{
		connected = i<j;
		weight = 1.0f;
		maxWt = 1.0f;
		delay = 1;
	}
};

//Customised spike generator class
class TemporalSpikes: public SpikeGenerator
{
	vector<int> spikeList, finishList;
public:
	TemporalSpikes(){};
	TemporalSpikes(vector<int> list)
	{
		for(int i=0; i<list.size(); i++)
		{
			spikeList.push_back(list[i]);
			finishList.push_back(0);
		}
	}
	unsigned int nextSpikeTime(CARLsim *sim, int grpId, int nid, unsigned int currentTime, unsigned int lastScheduledSpikeTime,
		unsigned int endOfTimeSlice)
	{
		if(finishList[nid] || (!spikeList[nid]))
			return SIMSECONDS * 1000 + SIMMILLISECONDS + 1;
		else
		{
			finishList[nid]++;
			return spikeList[nid];
		}
	}
};

class LateSpikes: public SpikeGenerator
{
	int time;
public:
	LateSpikes();
	LateSpikes(int time)
	{
		this->time = time;
	}
	unsigned int nextSpikeTime(CARLsim *sim, int grpId, int nid, unsigned int currentTime, unsigned int lastScheduledSpikeTime,
		unsigned int endOfTimeSlice)
	{
		if(time<=lastScheduledSpikeTime)
			return SIMSECONDS * 1000 + SIMMILLISECONDS + 1;
		else
			return time;
	}
};

//Converts number to spike trains
vector<vector<int> > convertNumToSpike(int n, int *array)
{
	vector<vector<int> > spikeList;
	for(int i=0; i<n; i++)
	{
		vector<int> bitList;
		int temp = BITLIMIT, num = array[i], time = 1;
		while(temp>0)
		{
			bitList.push_back(num & 1);
			num = num >> 1;
			temp--;
		}
		for(int i=0; i<bitList.size()/2; i++)
		{
			int temp = bitList[i];
			bitList[i] = bitList[bitList.size() - i - 1];
			bitList[bitList.size() - i - 1] = temp;
		}
		spikeList.push_back(bitList);
	}
	return spikeList;
}

int main(int argc, char **argv) {
	//Validate the arguments
	stringstream rowstream;
	if(argc == 1)
		return 1;

	const int n = argc-1;
	
	//Parameters for initilaising the simulator
	//Note: ithGPU isn't needed if the simulator is running in CPU mode
	//Also note from the CARLsim documentation that except for the first argument
	//all the other arguments have a default argument
	int ithGPU = 1;
	int randSeed = 42;
	CARLsim sim("Chen-Hsieh sort",CPU_MODE,USER,ithGPU,randSeed);

	//Declare the connection groups for connecting neuron groups in the future
	InhibConnection inhibConn;
	ExcitConnection excitConn;
	RankConnection rankConn;
	RowConnection rowConn;
	FinalConnection finConn[n];

	//Setup excitatory and inhibitory input groups
	/*int excInpGp, inhInpGp;
	excInpGp = sim.createSpikeGeneratorGroup("input", n, EXCITATORY_NEURON);
	sim.setNeuronParameters(excInpGp, 0.02f, 0.2f, -65.0f, 8.0f);
	inhInpGp = sim.createSpikeGeneratorGroup("inhibitor input", n, INHIBITORY_NEURON);
	sim.setNeuronParameters(inhInpGp, 0.02f, 0.2f, -65.0f, 8.0f);*/

	int numList[n], inhibNumList[n];
	int compLayer[n][n-1], inhibCompLayer[n][n-1];
	int selectionLayer[n][n-1];
	int comparator[n], pre_rank[n], inhib_pre_rank[n];
	int rank[n], selection[n], numbers[n];
	int inter_spike;
	inter_spike = sim.createSpikeGeneratorGroup("Exciter Neuron" + rowstream.str(), 1, EXCITATORY_NEURON);
	sim.setNeuronParameters(inter_spike, 0.02f, 0.2f, -65.0f, 8.0f);
	for(int i=0; i<n; i++)
		numbers[i] = atoi(argv[i+1]);

	//This loop provides the main connections between the input and the comparator matrix,
	//the comparator matrix and the memory matrix, the memory matrix and the synchronised comparator
	//matrix and the synchronised comparator matrix and the output matrix.
	for(int i=0; i<n; i++)
	{
		stringstream rowstream;
		rowstream<<i;

		//Create the required neuron groups
		finConn[i] = FinalConnection(i);
		numList[i] = sim.createSpikeGeneratorGroup("Number " + rowstream.str(), BITLIMIT, EXCITATORY_NEURON);
		inhibNumList[i] = sim.createGroup("Inhibitory Number " + rowstream.str(), BITLIMIT, INHIBITORY_NEURON);
		comparator[i] = sim.createGroup("Comparator-Matrix " + rowstream.str(), n, EXCITATORY_NEURON);
		pre_rank[i] = sim.createGroup("Pre-Rank-Layer " + rowstream.str(), n, EXCITATORY_NEURON);
		rank[i] = sim.createGroup("Rank " + rowstream.str(), n, EXCITATORY_NEURON);
		selection[i] = sim.createGroup("Selection-Layer " + rowstream.str(), n, EXCITATORY_NEURON);
		inhib_pre_rank[i] = sim.createGroup("Inhibitory-Pre-Rank-Layer " + rowstream.str(), n, INHIBITORY_NEURON);
		for(int j=0; j<n-1; j++)
		{
			compLayer[i][j] = sim.createGroup("Comparator Layer " + rowstream.str(), BITLIMIT, EXCITATORY_NEURON);
			inhibCompLayer[i][j] = sim.createGroup("Inhibitory Comp Layer " + rowstream.str(), BITLIMIT, INHIBITORY_NEURON);
			selectionLayer[i][j] = sim.createGroup("Selection Matrix " + rowstream.str(), BITLIMIT, EXCITATORY_NEURON);

			sim.setNeuronParameters(compLayer[i][j], 0.02f, 0.2f, -65.0f, 8.0f);
			sim.setNeuronParameters(inhibCompLayer[i][j], 0.02f, 0.2f, -65.0f, 8.0f);
			sim.setNeuronParameters(selectionLayer[i][j], 0.02f, 0.2f, -65.0f, 8.0f);
		}

		//set the parameters for the neurons
		sim.setNeuronParameters(numList[i], 0.02f, 0.2f, -65.0f, 8.0f);
		sim.setNeuronParameters(inhibNumList[i], 0.02f, 0.2f, -65.0f, 8.0f);
		sim.setNeuronParameters(comparator[i], 0.02f, 0.2f, -65.0f, 8.0f);
		sim.setNeuronParameters(pre_rank[i], 0.02f, 0.2f, -65.0f, 8.0f);
		sim.setNeuronParameters(rank[i], 0.02f, 0.21f, -65.0f, 8.0f);
		sim.setNeuronParameters(selection[i], 0.02f, 0.2f, -65.0f, 8.0f);
		sim.setNeuronParameters(inhib_pre_rank[i], 0.02f, 0.2f, -65.0f, 8.0f);

		//connect numList with inhibitory numlist;
		sim.connect(numList[i], inhibNumList[i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);

		//connect comparator and pre-rank matrix
		sim.connect(comparator[i], pre_rank[i], &rankConn, SYN_FIXED);

		//connect pre-rank matrix with rank matrix
		sim.connect(pre_rank[i], rank[i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
		sim.connect(pre_rank[i], inhib_pre_rank[i], "one-to-one", RangeWeight(1.0f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
		sim.connect(inhib_pre_rank[i], pre_rank[i], "one-to-one", RangeWeight(1.0f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
		sim.connect(inhib_pre_rank[i], rank[i], &inhibConn, SYN_FIXED);

		//connect spike-generator to pre-rank matrix
		sim.connect(inter_spike, pre_rank[i], &excitConn, SYN_FIXED);
	}

	//This loop connects the negative intermediate matrix with the comparator matrix
	//to prevent any future spikes from the matrix. It also connects the synchronised
	//comparator matrix with the rank matrix and the (i+1)th row of rank matrix with
	//the ith row of selection matrix.
	for(int i=0; i<n; i++)
	{
		for(int j=i+1; j<n; j++)
		{
			sim.connect(numList[i], compLayer[i][j-1], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
			sim.connect(numList[j], compLayer[j][i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(6), RadiusRF(-1), SYN_FIXED);
			sim.connect(inhibNumList[i], compLayer[j][i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1), RadiusRF(-1), SYN_FIXED);
			sim.connect(inhibNumList[j], compLayer[i][j-1], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1),
				RadiusRF(-1), SYN_FIXED);
			sim.connect(compLayer[i][j-1], inhibCompLayer[i][j-1], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1),
				RadiusRF(-1), SYN_FIXED);
			sim.connect(compLayer[j][i], inhibCompLayer[j][i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(1),
				RadiusRF(-1), SYN_FIXED);
			sim.connect(compLayer[i][j-1], selectionLayer[i][j-1], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(6),
				RadiusRF(-1), SYN_FIXED);
			sim.connect(compLayer[j][i], selectionLayer[j][i], "one-to-one", RangeWeight(0.4f), 1.0f, RangeDelay(6),
				RadiusRF(-1), SYN_FIXED);
			sim.connect(inhibCompLayer[i][j-1], selectionLayer[j][i], &rowConn, SYN_FIXED);
			sim.connect(inhibCompLayer[j][i], selectionLayer[i][j-1], &rowConn, SYN_FIXED);
			sim.connect(selectionLayer[i][j-1], comparator[i], &finConn[j], SYN_FIXED);
			sim.connect(selectionLayer[j][i], comparator[j], &finConn[i], SYN_FIXED);
		}
	}

	//This loop is responsible for connecting all the gate neurons in a row to the
	//corresponding adder neuron in the adder matrix.
	sim.setConductances(true);

	//convert input numbers to spikes
	vector<vector<int> > testSpike;
	testSpike = convertNumToSpike(n, numbers);
	TemporalSpikes excSpike[n];
	LateSpikes late(28);
	for(int i=0; i<n; i++)
	{
		excSpike[i] = TemporalSpikes(testSpike[i]);
		sim.setSpikeGenerator(numList[i], &excSpike[i]);
	}
	sim.setSpikeGenerator(inter_spike, &late);

	//setup the neurons, the connections and the generators.
	sim.setupNetwork();
	sim.setConnectionMonitor(inhib_pre_rank[2], pre_rank[2], "dump");

	// set some spike monitors
	SpikeMonitor *spkMonout[n];
	for(int i=0; i<n; i++)
		spkMonout[i] = sim.setSpikeMonitor(rank[i], "DEFAULT");

	// run for 1 second
	// at the end of runNetwork call, SpikeMonitor stats will be printed
	for(int i=0; i<n; i++)
		spkMonout[i]->startRecording();
	cout<<"Finished Recording\n";
	sim.runNetwork(SIMSECONDS, SIMMILLISECONDS);
	for(int i=0; i<n; i++)
		spkMonout[i]->stopRecording();
	printf("Selection\n");
	for(int i=0; i<n; i++)
	{
		cout<<"Row - "<<i+1<<endl;
		vector<vector<int> > output = spkMonout[i]->getSpikeVector2D();
		for(int j=0; j<output.size(); j++)
		{
			vector<int> spikeTime = output[j];
			for(int k=0; k<spikeTime.size(); k++)
				printf("%d ", spikeTime[k]);
			printf("\n");
		}
	}
	return 0;
}
