fileName = input("Spike data filename: ", 's');
fid = fopen(fileName)
neuronId = 1;
plotArray = [];
tempArray = fgetl(fid);
while(!feof(fid))
	tempArray = sscanf(tempArray, '%d')';
	if(length(tempArray) > 0)
		spikeArray = [tempArray; (ones(1, length(tempArray)) * neuronId)];
		plotArray = [plotArray spikeArray];
		neuronId = neuronId + 1;
	end
	tempArray = fgetl(fid);
end
fclose(fid);
plotArray = plotArray(1, :);
[freq, num] = hist(plotArray, 1:10:1000);
fileName = input("histogram data filename: ", 's');
freq  = freq';
num = num';
fid = fopen(fileName, 'w');
for i = 1:length(freq)
	str = [num2str(freq(i)) ',' num2str(num(i)) "\n"];
	fputs(fid, str);
end
fclose(fid);
